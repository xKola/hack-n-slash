﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransparencyController : MonoBehaviour
{
    public Material transparentMaterial;

    public List<Material> currentMaterials;

    public bool hitLastFrame;

    private Renderer _matRenderer;

    [Range(0f, 1f)]
    public float maxTransparencyValue = 1f, minTransparencyValue = 0f;

    public float fadeOutSpeed = 3f, fadeInSpeed = 3f;

    private string _transparencyShaderName = "_Tr";

    // Start is called before the first frame update
    private void Start()
    {
        _matRenderer = GetComponent<Renderer>();

        currentMaterials = new List<Material>(_matRenderer.materials);
    }

    // Update is called once per frame
    private void Update()
    {
        if (hitLastFrame)
        {
            foreach (var mat in currentMaterials.Where(mat => mat.HasProperty(_transparencyShaderName)))
            {
                mat.SetFloat(_transparencyShaderName, Mathf.Lerp(mat.GetFloat(_transparencyShaderName), maxTransparencyValue, Time.deltaTime * fadeOutSpeed));
            }
        }
        else
        {
            if (_matRenderer.material.GetFloat(_transparencyShaderName) > minTransparencyValue)
            {
                foreach (var mat in currentMaterials.Where(mat => mat.HasProperty(_transparencyShaderName)))
                {
                    mat.SetFloat(_transparencyShaderName, Mathf.Lerp(mat.GetFloat(_transparencyShaderName), minTransparencyValue, Time.deltaTime * fadeInSpeed));
                }
            }
        }

        _matRenderer.materials = currentMaterials.ToArray();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            hitLastFrame = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            hitLastFrame = false;
        }
    }
}
