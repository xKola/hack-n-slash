﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CombatController : MonoBehaviour
{
    //public GameObject focusedEnemy;
    public Animator animator;
    public float weaponRange;

    public bool duringAttack = false;
    public  bool checkIfInRange = false;
    public GameObject _focusedEnemy;

    private NavMeshAgent _mNavMeshAgent;

    public GameObject weaponHold;
    public GameObject currentWeapon;

    // Start is called before the first frame update
    void Start()
    {
        _mNavMeshAgent = GameManager.Instance.playerMovementController.meshAgent;
        //SetWeapon();

    }

    // Update is called once per frame
    void Update()
    {

        if (checkIfInRange)
        {
            if (_focusedEnemy && (_focusedEnemy.transform.position - transform.position).magnitude - 0.1f < weaponRange)
            {
                // Done
                PerformAttackOn(_focusedEnemy);
                checkIfInRange = false;
            }
            if (!_mNavMeshAgent.pathPending)
            {
                if (_mNavMeshAgent.remainingDistance <= _mNavMeshAgent.stoppingDistance)
                {
                    if (!_mNavMeshAgent.hasPath || _mNavMeshAgent.velocity.sqrMagnitude == 0f)
                    {
                        // Done
                        PerformAttackOn(_focusedEnemy);
                        checkIfInRange = false;
                    }
                }
            }
        }    
    }

    public void PerformAttackOn(GameObject focusedEnemy)
    {
        _focusedEnemy = focusedEnemy;
        if (!duringAttack && (focusedEnemy.transform.position - transform.position).magnitude - 0.1f < weaponRange)
        {
            //Debug.Log("I am attacking");
            _mNavMeshAgent.ResetPath();
            animator.SetBool("Attack", true);
            duringAttack = true;
            transform.rotation = Quaternion.LookRotation(focusedEnemy.transform.position - transform.position);
        }
        else if (!duringAttack && (focusedEnemy.transform.position - transform.position).magnitude - 0.1f >= weaponRange)
        {
            //animator.SetBool("Attack", false);
            GameManager.Instance.SetPlayerDestination(focusedEnemy.transform.position);
            checkIfInRange = true;
        }
    }

    public void SetWeapon(GameObject weaponObject)
    {
        //currentWeapon = Instantiate(SwapWeapon.Instance.weapons[0].gameObject);
        if (currentWeapon)
        {
            Destroy(currentWeapon);
        }
        currentWeapon = Instantiate(weaponObject);
        currentWeapon.transform.parent = weaponHold.transform;
        currentWeapon.transform.localPosition = currentWeapon.GetComponent<Weapon>().offsetPosition;
        currentWeapon.transform.localRotation = currentWeapon.GetComponent<Weapon>().rotation;
        currentWeapon.transform.localScale = currentWeapon.GetComponent<Weapon>().scale;
        weaponRange = currentWeapon.GetComponent<Weapon>().range;
    }

    public void UnfocusEnemy()
    {
        animator.SetBool("Attack", false);
        _focusedEnemy = null;
        duringAttack = false;
    }

    public void FinishedAnimation()
    {
        animator.SetBool("Attack", false);
        duringAttack = false;
        _focusedEnemy.GetComponent<Enemy>().GetDamage(10f);
    }

}
