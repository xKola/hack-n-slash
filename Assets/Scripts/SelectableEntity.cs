﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableEntity : MonoBehaviour
{
    private SphereWithMaterialPropertyBlock[] materials;
    public float power = 1f, effectSpeed = 2f;

    public Color[] originalColors, newColorsToSet;

    private bool highlighted;

    void Start()
    {
        materials = GetComponentsInChildren<SphereWithMaterialPropertyBlock>();
        originalColors = new Color[materials.Length];
        newColorsToSet = new Color[materials.Length];

        for (int i = 0; i < materials.Length; i++)
        {
            originalColors[i] = materials[i].newColor;
        }        
    }

    void Update()
    {
        for (int i = 0; i < materials.Length; i++)
        {
            newColorsToSet[i] = originalColors[i] * power;
        }

        if (!highlighted)
        {
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i].newColor = Color.Lerp(materials[i].newColor, originalColors[i], Time.deltaTime * effectSpeed);
            }
        }
    }

    void OnMouseOver()
    {
        highlighted = true;

        for (int i = 0; i < materials.Length; i++)
        {
            materials[i].newColor = Color.Lerp(materials[i].newColor, newColorsToSet[i], Time.deltaTime * effectSpeed);
        }
    }

    private void OnMouseExit()
    {
        highlighted = false;
    }
}
