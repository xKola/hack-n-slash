﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Enemy : MonoBehaviour
{
    public float maxHP, currentHP;
    public string entityName;

    public Image _barHP;
    public float fillSpeed;
    public TextMeshProUGUI entityNameText;

    private SelectableEntity selectableEntityScript;

    public float radius = 1;

    void Start()
    {
        selectableEntityScript = GetComponent<SelectableEntity>();
        entityNameText.text = entityName;
        currentHP = maxHP;
    }

    // Update is called once per frame
    void Update()
    {
         _barHP.fillAmount = Mathf.Lerp(_barHP.fillAmount, currentHP / maxHP, Time.deltaTime * fillSpeed);
    }

    private void OnMouseDown()
    {
        GameManager.Instance.playerCombatController.PerformAttackOn(gameObject);
    }

    public void GetDamage(float dmg)
    {
        currentHP -= dmg;
    }
}
