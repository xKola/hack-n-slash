﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    public NavMeshAgent meshAgent;
    private Animator _animator;
    private Camera _camera;

    // Start is called before the first frame update
    private void Start()
    {
        _camera = Camera.main;
        meshAgent.updateRotation = false;
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray mousePos = _camera.ScreenPointToRay(Input.mousePosition);

            Physics.Raycast(mousePos, out var hit);
            //Debug.DrawRay(_camera.transform.position, mousePos.direction * 30f, Color.red, 2f);

            if (hit.transform && hit.transform.gameObject.layer == 9)
            {
                meshAgent.destination = hit.point;
                GameManager.Instance.playerCombatController.UnfocusEnemy();
            }
        }
        
    }

    private void LateUpdate()
    {
        if (meshAgent.velocity.sqrMagnitude > Mathf.Epsilon)
        {
            transform.rotation = Quaternion.LookRotation(meshAgent.velocity.normalized);
            _animator.SetBool("Run", true);
        } else
        {
            _animator.SetBool("Run", false);
        }
    }
}
