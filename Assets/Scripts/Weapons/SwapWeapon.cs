﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapWeapon : MonoBehaviour
{
    private static SwapWeapon _instance;

    public Weapon[] weapons;

    public Animator animator;
    public AnimatorOverrideController animatorOverrideController;

    protected int weaponIndex;

    protected AnimationClipOverrides clipOverrides;

    public static SwapWeapon Instance { get { return _instance; } }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        //animator = GameObject.Find("Player").GetComponent<Animator>();
        //animator = GetComponent<Animator>();
        weaponIndex = 0;

        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;

        clipOverrides = new AnimationClipOverrides(animatorOverrideController.overridesCount);
        animatorOverrideController.GetOverrides(clipOverrides);

        GameManager.Instance.playerCombatController.SetWeapon(weapons[weaponIndex].gameObject);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            weaponIndex = (weaponIndex + 1) % weapons.Length;
            clipOverrides["Sword Attack"] = weapons[weaponIndex].attackAnimation;
            animatorOverrideController.ApplyOverrides(clipOverrides);
            GameManager.Instance.playerCombatController.SetWeapon(weapons[weaponIndex].gameObject);
        }
    }
}