﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public AnimationClip attackAnimation;
    public Vector3 offsetPosition;
    public Vector3 scale;
    public Quaternion rotation;
    public float range;
}
