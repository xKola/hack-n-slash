﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransparencyEntity : MonoBehaviour
{
    public Material transparentMaterial;

    public List<Material> currentMaterials;
    public List<Material> originalMaterials;

    public bool fadeOut = false, hitLastFrame = false;

    private Renderer matRenderer;

    [Range(0, 1f)]
    public float transparencyValue;

    public float fadeSpeed;

    // Start is called before the first frame update
    void Start()
    {
        matRenderer = GetComponent<Renderer>();

        currentMaterials = new List<Material>(matRenderer.materials);
        originalMaterials = new List<Material>(matRenderer.materials);
    }

    // Update is called once per frame
    void Update()
    {
        if (hitLastFrame)
        {
            foreach (Material mat in currentMaterials.Where(x => x.HasProperty("_Tr")))
            {
                //mat.color = Color.Lerp(mat.color, new Color(mat.color.r, mat.color.g, mat.color.b, transparencyValue), Time.deltaTime * fadeSpeed);
                mat.SetFloat("_Tr", Mathf.Lerp(mat.GetFloat("_Tr"), transparencyValue, Time.deltaTime * fadeSpeed));
                Debug.Log(mat.name);
            }
        }
        else
        {
            foreach (Material mat in matRenderer.materials.Where(x => x.HasProperty("_Tr")))
            {
                mat.SetFloat("_Tr", Mathf.Lerp(mat.GetFloat("_Tr"), 0, Time.deltaTime * fadeSpeed));
            }
        }

        matRenderer.materials = currentMaterials.ToArray();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            hitLastFrame = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            hitLastFrame = false;
        }
    }
}
