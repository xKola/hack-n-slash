﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public GameObject player; //{ get { return player; } private set { } }

    public MovementController playerMovementController;

    public CombatController playerCombatController;

    public static GameManager Instance { get { return _instance; } }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);


        player = GameObject.Find("Player");
        playerMovementController = player.GetComponent<MovementController>();
        playerCombatController = player.GetComponent<CombatController>();
    }

    public void SetPlayerDestination(Vector3 destinationPoint)
    {
        
        playerMovementController.meshAgent.SetDestination(destinationPoint + (player.transform.position - destinationPoint).normalized * playerCombatController.weaponRange);
        //Debug.Log($"Destination set to: {destinationPoint.normalized}");
    }

    public void SetPlayerFocus(GameObject toFocus)
    {
        //playerCombatController.focusedEnemy = toFocus;
        //playerCombatController.PerformAttackOn();
    }
}